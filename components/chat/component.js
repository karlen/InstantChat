﻿var Chat_component = {

    showLoader:true,
    LoaderColor:'#4774A8',
    background_color :'white',

    all_modules : [

        {
            module_name: 'in_user',
            position: '.leftpanel .in_user',
            TplData: JSON.parse(Storage.get('user')),
            OtherData: {},
            widgets: [],
            success: function () {
            }
        },
        {
            module_name: 'search',
            position: '.leftpanel .search',
            TplData: JSON.parse(Storage.get('user')),
            OtherData: {},
            widgets: [],
            success: function () {

            }
        },
        {
            module_name: 'dialogs',
            position: '.leftpanel .dialogs',
            TplData: JSON.parse(Storage.get('contacts')),
            OtherData: {},
            widgets: [],
            success: function () {
                $('.leftpanel .dialogs .contact').scaleAble();
            }
        },
        {
            module_name: 'profile',
            position: '.main',
            TplData: {},
            OtherData: {},
            widgets: [
                {
                    widget_name: 'top',
                    position: '.main .top',
                    TplData: {'nickname': JSON.parse(Storage.get('user')).nickname},
                    OtherData: {},
                    success: function () {
                    }
                },
                {
                    widget_name: 'user_data',
                    position: '.main .blocks .first_block',
                    TplData: JSON.parse(Storage.get('user')),
                    OtherData: {},
                    success: function () {
                    }
                }
                ,
                {
                    widget_name: 'user_friends',
                    position: '.main .blocks .second_block .friends',
                    TplData: {},
                    OtherData: {friends: JSON.parse(Storage.get('friends'))},
                    success: function () {

                    }
                },
                {
                    widget_name: 'user_wall',
                    position: '.main .blocks .second_block .wall',
                    TplData: {},
                    OtherData: {wall: JSON.parse(Storage.get('wall')), smilies: MainApp.getSmilies()},
                    success: function () {


                    }
                }
            ],
            success: function () {

            }
        }


    ],

    Init: function () {



       /* var win = gui.Window.get([document.window]);  // If window_object is not specifed, then return current window's Window object, otherwise return window_object's Window object.
        win.maximize();*/






    }


}