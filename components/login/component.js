﻿var Login_component = {

    showLoader:true,
    LoaderColor:'#4774A8',
    background_color :'#4774A8',
    all_modules : [],


    Init: function () {

       /* var win = gui.Window.get([document.window]);  // If window_object is not specifed, then return current window's Window object, otherwise return window_object's Window object.

        win.unmaximize();*/
        MainApp.changeActivePage('login');

        $('body').css('background-color', '#4774A8');


        

        $('.login_wrap .login_form').keyup(function(e) {
            if(e.which == 13) {

                $('.login_wrap .login_form .submit_login').click();

            }
        });


        $('.login_wrap .login_form .submit_login').kt(function () {


            if ($('.login_wrap .login_form progress').css('visibility') == 'hidden') {


                var mail = $('.login_wrap .login_form .user_login').val(),
                    password = $('.login_wrap .login_form .user_password').val(),
                    VRegExp = new RegExp(/^(\s|\u00A0)+/g);

                mail = mail.replace(VRegExp, '');
                password = password.replace(VRegExp, '');


                if (mail.length <= 0 || password.length <= 0) {

                    navigator.notification.alert("Пожалуйста заполните необходимые поля", function () {
                    }, " ");

                }


                else if (mail.length <= 4 || password.length <= 5) {

                    navigator.notification.alert("Некорректные данные", function () {
                    }, " ");

                }

                else {

                    $('.login_wrap .login_form progress').css('visibility', 'visible');
                    $('.login_wrap .login_form .user_login , .login_wrap .login_form .user_password').attr('readonly', 'readonly');


                    MainApp.SendData('do=login&login=' + mail + '&password=' + password, function (data) {



                        

                        var VRegExp = new RegExp(/^(\s|\u00A0)+/g);


                        if (data.replace(VRegExp, '') == "false") {

                            $('.login_wrap .login_form progress').css('visibility', 'hidden');
                            $('.login_wrap .login_form .user_login , .login_wrap .login_form .user_password').removeAttr('readonly');


                            navigator.notification.alert("Неверный логин или пароль", function () {
                            }, " ");


                        }
                        else {

                            try {
                                var user_data = JSON.parse(data);
                                Storage.add("user", data);
                                Storage.add("token", user_data['token']);

                                $.get(MainApp.HostUrl + user_data['avatar'], function () {


                                    $('.login_wrap .login_form .logo').addClass('rounded').css('background-image', 'url(' + MainApp.HostUrl + user_data['avatar'] + ')');

                                    $('.login_wrap .login_form input').remove();
                                    $('.login_wrap .login_form progress').css('margin-top', '64px');

                                    MainApp.SendData('do=getcontacts&token=' + user_data['token'], function (allData) {

                                        try {

                                            allData = JSON.parse(allData);

                                            dataChat = allData[0];
                                            dataFriends = allData[1];
                                            dataWall = allData[2];
                                            notifications = allData[3];

                                            

                                            Storage.add("contacts", dataChat);
                                            Storage.add("friends", dataFriends);
                                            Storage.add("wall", dataWall);
                                            Storage.add("notifications", notifications);

                                            MainApp.loadComponent('chat', {}, {}, function () {

                                                setTimeout(function () {
                                                    User.GetNewMessages();
                                                }, 5000);

                                                MainApp.ShowNotifications();

                                            });


                                        }
                                        catch (e) {
                                            navigator.notification.alert("Проблема с сервером", function () {
                                                MainApp.loadComponent('login', {}, {}, function () {



                                                });
                                            }, " ");


                                        }


                                    }, function () {

                                        navigator.notification.alert('Ошибка соединения', function () {
                                            MainApp.loadComponent('login', {}, {}, function () {



                                            });
                                        }, " ");

                                    });


                                });

                            }
                            catch (e) {
                                $('.login_wrap .login_form progress').css('visibility', 'hidden');
                                $('.login_wrap .login_form .user_login , .login_wrap .login_form .user_password').removeAttr('readonly');
                                navigator.notification.alert("Проблема с сервером", function () {
                                }, " ");


                            }

                        }

                    }, function () {

                        navigator.notification.alert('Ошибка соединения', function () {
                            $('.login_wrap .login_form progress').css('visibility', 'hidden');
                            $('.login_wrap .login_form .user_login , .login_wrap .login_form .user_password').removeAttr('readonly');
                        }, " ");


                    });

                }

            }

        });

    }


};










