﻿var In_User_module = {

    Init :function(){


            $('.leftpanel .in_user .user_wrap').kt(function(e,obj){



                if(MainApp.ActivePage != 'profile')
                {
                    MainApp.ShowModuleLoader();



               var module =  [{
                    module_name: 'profile',
                        position: '.main',
                    TplData: {},
                    OtherData: {},
                    widgets: [
                        {
                            widget_name: 'top',
                            position: '.main .top',
                            TplData: {'nickname': JSON.parse(Storage.get('user')).nickname},
                            OtherData: {},
                            success: function () {
                            }
                        },
                        {
                            widget_name: 'user_data',
                            position: '.main .blocks .first_block',
                            TplData: JSON.parse(Storage.get('user')),
                            OtherData: {},
                            success: function () {
                            }
                        }
                        ,
                        {
                            widget_name: 'user_friends',
                            position: '.main .blocks .second_block .friends',
                            TplData: {},
                            OtherData: {friends: JSON.parse(Storage.get('friends'))},
                            success: function () {

                            }
                        },
                        {
                            widget_name: 'user_wall',
                            position: '.main .blocks .second_block .wall',
                            TplData: {},
                            OtherData: {wall: JSON.parse(Storage.get('wall')), smilies: MainApp.getSmilies()},
                            success: function () {


                            }
                        }
                    ],
                        success: function () {

                }
               }];

                MainApp.foreachForModules(0,module,function(){

                    MainApp.HideModuleLoader();

                    $('[data-animation]').each(function () {

                        $(this).addClass($(this).attr('data-animation'));

                    });


                    $('[data-scale = true]').scaleAble();
                    $('.active').removeClass('active');
                    obj.addClass('active');


                });


                }



            });


    }


}

In_User_module.Init();
