﻿var Messages_module = {



    GetImageFromFile:function(){


        $('#file_picker').remove();

        var input=document.createElement('input');
        input.type="file";
        input.id = "file_picker";
        input.hidden = 'hidden';
        input.accept= "image/x-png, image/gif, image/jpeg";
        document.getElementsByTagName('body')[0].appendChild(input);


        $(input).click();

        $(input).on('change',function(evt){
            var file = evt.target.files[0];
            Messages.SendImageMessage(file,1);




        });




    },


    GetImageFromCamera:function(){

        navigator.camera.getPicture(function (picture) {




              if (picture) {

                  Messages.SendImageMessage(picture,0);

              }
        });





    },





    Init: function (Contact_id) {



        var smiles = MainApp.getSmilies();
        var count = Math.ceil(Object.keys(smiles).length / 2);
        var wdth = 50 * count;

        $('.main .input_box .smilies > div').css('width', wdth + 'px');


        $(".main .input_box .smilies").mousewheel(function (event, delta) {

            var sc = $('.main .input_box .smilies').scrollLeft();
            sc -= (delta * 15);

            $('.main .input_box .smilies').scrollLeft(sc);

            event.preventDefault();

        });


        $('.main .input_box .smilies > div .smile_it').kt(function (a, b) {

        var messgae = $('.main .input_box .text_message').html();
        var smile = '<img unselectable="on" src="' + b.attr('data-url') + '" />';
        messgae += smile;
        $('.main .input_box .text_message').html(messgae);

        });

        $('.main  [data-animation]').each(function () {

            $(this).addClass($(this).attr('data-animation'));

        });



        $('.main  [data-scale = true]').scaleAble();

        $('.main .input_box .text_message').attr('contenteditable', 'true');
        

        $('.main .input_box .text_message').on('keydown keyup input DOMNodeInserted DOMNodeRemoved', function () {
           
            if (!$(this).html()) {
                $(this).attr('data-placeholder', 'Введите текст сообщения...');
            }
            else {
                $(this).removeAttr('data-placeholder');
               
            }
        });


        $('.main .input_box .text_message').keydown(function(e) {
            if(e.ctrlKey && e.which == 13) {

                $('.main .input_box .send').click();

            }
        });


        $('.main .messages_box .openleftpanel').click(function (a) {

            a.stopPropagation();

            var is_opened = $('.main .messages_box .openleftpanel').attr('data-opened');
            if(is_opened=='0')
            {
                Messages_module.openLeftPanel();
            }
            else
            {
                Messages_module.closeLeftPanel();
            }



        });



        $('.main .input_box .send').kt(function (a,b) {

            var message = $('.main .input_box .text_message').html();

            Messages.SendMessage(message);

        });



        $('.main .input_box .attach').click(function (clickArgs, obj) {



            var firstContext = {'title': 'Из файлов', 'function': 'Messages_module.GetImageFromFile()'};
            var secondContext = {'title': 'C камеры', 'function': 'Messages_module.GetImageFromCamera()'};


            var Context = [firstContext,secondContext];


            navigator.popup.showPopUp(this, Context, 'top');









        });



        Messages_module.ShowMessages(Contact_id);


    },

    ShowMessages: function (Contact_id) {
        $('.messages_box .messages_list .progress_bar').removeClass('hiddenProgress');
        var messages = Messages.GetMessages(Contact_id, function (messages_data) {


            if (MainApp.ActivePage == 'Message_' + Contact_id) {

                if (JSON.stringify(messages_data) != '[]' && JSON.stringify(messages_data) != '{"data":"empty"}') {

                    MainApp.loadTemplate('modules/messages/messages.html', messages_data, { in_us_id: JSON.parse(Storage.get('user')).id, hvr: 'hvr-push' }, '.main .messages_box .messages_list .show_messages', 'html', function () {





                        $('.messages_box .messages_list .progress_bar').addClass('hiddenProgress');
                        $('.main .messages_box .messages_list .show_messages').scrollTop($('.main .messages_box .messages_list .show_messages')[0].scrollHeight);









                    });
                }
                else {

                    $('.messages_box .messages_list .progress_bar').addClass('hiddenProgress');
                    $(".main .messages_box .messages_list .show_messages").css('background-image', 'url("images/dialog.png")');

                }

            }


        });

    }

};

