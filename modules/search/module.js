﻿var Search_module = {
    Init:function(){

        $('.leftpanel .search input').click(function(a){
            a.stopPropagation();
        });


        $('.leftpanel .search input').on('input', function () {


            var text = $('.leftpanel .search input').val();

            text = text.toLowerCase();

            $('.leftpanel .dialogs .contact').each(function () {


                var user_name = $('.nickname', this).text();
                user_name = user_name.toLowerCase();
                if (user_name.match(text)) {
                    $(this).show();
                    
                }
                else {

                    $(this).hide(200);

                }


            });
        

        });
    }
}

Search_module.Init();


