﻿var users = {

   

    acceptFriend: function (user_id, message_id) {

       
        MainApp.ShowAppLoader();
        
        MainApp.SendData('do=addfriend&id=' + user_id + '&message_id=' + message_id, function (response) {

            MainApp.HideAppLoader();

            $('.notifications_wrap .notification_it[data-message='+message_id+']').slideUp(700);

        }, function () {



        });

    },
    rejectFriend: function (user_id, message_id) {
        MainApp.ShowAppLoader();

        MainApp.SendData('do=rejectfriend&id=' + user_id + '&message_id=' + message_id, function (response) {

            MainApp.HideAppLoader();

            $('.notifications_wrap .notification_it[data-message=' + message_id + ']').slideUp(700);

        }, function () {



        });
    }


};

var MainApp = {
    ActivePage: null,
    HostUrl: 'http://app',

    InitPage: function () {

       
  
       
        

       

        $.views.helpers({
            HostUrl: MainApp.HostUrl,
            getimgsrc: function (value) {

                return 'src="' + MainApp.HostUrl + value + '"';

            },
            getimgurl: function (value) {

                return '  style="background-image: url(' + MainApp.HostUrl + value + ');"';

            }


        });

        MainApp.showMainLoader();

        User.LoginUser(function () {

            MainApp.loadComponent('chat', {}, {}, function () {


                            
                setTimeout(function () {
                    User.GetNewMessages();
                }, 5000);

                MainApp.ShowNotifications();

               


            });


        }, function () {

            MainApp.loadComponent('login', {}, {}, function () {

                for (var i = 0; i < 10; i++) {
                    clearTimeout(i);
                    clearInterval(i);
                }
                
             


            });

        });
          


    },

    parseNotifications:function(notification,message_id)
    {
        

        
        var elem = document.createElement("div");
        elem.innerHTML = notification;

        

        $('a', elem).each(function () {


            if ($(this).attr('href') && !$(this).hasClass('ajaxlink')) {

                var src = $(this).attr('href');
                $(this).attr('href', MainApp.HostUrl + src);
                $(this).attr('data-messageid', message_id);

            }
            else {

                var a = $(this).attr('x-onclick');
               
                $(this).removeAttr('x-onclick');
                $(this).attr('data-friend', a);
                $(this).attr('data-messageid', message_id);

            }


            

        });



      
        
        
        return elem.innerHTML;
    },


    realLength: function (inputText) {
        var returnText = "" + inputText;

        //-- remove BR tags and replace them with line break
        returnText = returnText.replace(/<br>/gi, " ");
        returnText = returnText.replace(/<br\s\/>/gi, " ");
        returnText = returnText.replace(/<br\/>/gi, " ");


        //-- remove BR tags and replace them with line break
        returnText = returnText.replace(/<div>/gi, " ");
        returnText = returnText.replace(/<div\s\/>/gi, " ");
        returnText = returnText.replace(/<div\/>/gi, " ");

        //-- remove BR tags and replace them with line break
        returnText = returnText.replace(/<\/div>/gi, " ");
        returnText = returnText.replace(/<\/div\s\/>/gi, " ");
        returnText = returnText.replace(/<\/div\/>/gi, " ");

        //-- remove P and A tags but preserve what's inside of them
        returnText = returnText.replace(/<p.*>/gi, " ");
        returnText = returnText.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 ($1)");

        //-- remove all inside SCRIPT and STYLE tags
        returnText = returnText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
        returnText = returnText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
        //-- remove all else
        //returnText=returnText.replace(/<(?:.|\s)*?>/g, "");

        //-- get rid of more than 2 multiple line breaks:
        returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "  ");

        //-- get rid of more than 2 spaces:
        returnText = returnText.replace(/ +(?= )/g, '');

        //-- get rid of html-encoded characters:
        returnText = returnText.replace(/&nbsp;/gi, " ");
        returnText = returnText.replace(/&amp;/gi, "&");
        returnText = returnText.replace(/&quot;/gi, '"');
        returnText = returnText.replace(/&lt;/gi, '<');
        returnText = returnText.replace(/&gt;/gi, '>');


        returnText = returnText.replace(/^(\s|\u00A0)+/g, '');

        //-- return
        return returnText;
    },


    StripHtml: function (inputText) {
        var returnText = "" + inputText;


        //-- remove BR tags and replace them with line break
        returnText = returnText.replace(/<div>/gi, " ");
        returnText = returnText.replace(/<div\s\/>/gi, " ");
        returnText = returnText.replace(/<div\/>/gi, " ");

        //-- remove BR tags and replace them with line break
        returnText = returnText.replace(/<\/div>/gi, "<br>");
        returnText = returnText.replace(/<\/div\s\/>/gi, "<br>");
        returnText = returnText.replace(/<\/div\/>/gi, "<br>");

        returnText = returnText.replace(/<\/div><br><\/div>/gi, "<br>");
        returnText = returnText.replace(/<\/div\s\/><br><\/div>/gi, "<br>");
        returnText = returnText.replace(/<\/div\/><br><\/div>/gi, "<br>");

        //-- remove P and A tags but preserve what's inside of them
        returnText = returnText.replace(/<p.*>/gi, "\n");
        returnText = returnText.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 ($1)");

        //-- remove all inside SCRIPT and STYLE tags
        returnText = returnText.replace(/<script.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/script>/gi, "");
        returnText = returnText.replace(/<style.*>[\w\W]{1,}(.*?)[\w\W]{1,}<\/style>/gi, "");
        //-- remove all else
        //returnText=returnText.replace(/<(?:.|\s)*?>/g, "");

        //-- get rid of more than 2 multiple line breaks:
        returnText = returnText.replace(/(?:(?:\r\n|\r|\n)\s*){2,}/gim, "<br>");
        returnText = returnText.replace(/(?:(?:<br>|<br>)\s*){2,}/gim, "<br>");

        //-- get rid of more than 2 spaces:
        returnText = returnText.replace(/ +(?= )/g, '');


        returnText = returnText.replace(/\&nbsp\;/gi, ' ');


        //-- return
        return returnText;
    },


    changeActivePage: function (page) {
        MainApp.ActivePage = page;
        Storage.add('activePage', page);
    },

    showMainLoader: function (component, color) {


        $('.show_screen').css('background-color', color);
        $('.show_screen').show();

        if (component != 'login') {
            var avatar = JSON.parse(Storage.get('user')).avatar;
            MainApp.loadTemplate('mainloader.html', {}, { avatar: 'style="background-image: url(' + MainApp.HostUrl + avatar + ');"' }, '.show_screen', 'html', function () {


            });
        }
        else {

            
            MainApp.loadTemplate('mainloader.html', {}, { avatar: 'style="background-image: url(images/iconLogin.svg);"' }, '.show_screen', 'html', function () {


            });
        }

    },

    getSmilies: function () {

        var smilies = [
            {'name': 'angel'},
            {'name': 'crazy'},
            {'name': 'cry'},
            {'name': 'dance'},
            {'name': 'glasses'},
            {'name': 'hoho'},
            {'name': 'joke'},
            {'name': 'laugh'},
            {'name': 'look'},
            {'name': 'music'},
            {'name': 'rofl'},
            {'name': 'sad'},
            {'name': 'scratch'},
            {'name': 'shock'},
            {'name': 'sick'},
            {'name': 'smile'},
            {'name': 'smoke'},
            {'name': 'stuk'},
            {'name': 'v'},
            {'name': 'zlo'},
            {'name': 'angel'},
            {'name': 'crazy'},
            {'name': 'cry'},
            {'name': 'dance'},
            {'name': 'glasses'},
            {'name': 'hoho'},
            {'name': 'joke'},
            {'name': 'laugh'},
            {'name': 'look'},
            {'name': 'music'},
            {'name': 'rofl'},
            {'name': 'sad'},
            {'name': 'scratch'},
            {'name': 'shock'},
            {'name': 'sick'},
            {'name': 'smile'},
            {'name': 'smoke'},
            {'name': 'stuk'},
            {'name': 'v'},
            {'name': 'zlo'},
            {'name': 'angel'},
            {'name': 'crazy'},
            {'name': 'cry'},
            {'name': 'dance'},
            {'name': 'glasses'},
            {'name': 'hoho'},
            {'name': 'joke'},
            {'name': 'laugh'},
            {'name': 'look'},
            {'name': 'music'},
            {'name': 'rofl'},
            {'name': 'sad'},
            {'name': 'scratch'},
            {'name': 'shock'},
            {'name': 'sick'},
            {'name': 'smile'},
            {'name': 'smoke'},
            {'name': 'stuk'},
            {'name': 'v'},
            {'name': 'zlo'},
            {'name': 'angel'},
            {'name': 'crazy'},
            {'name': 'cry'},
            {'name': 'dance'},
            {'name': 'glasses'},
            {'name': 'hoho'},
            {'name': 'joke'},
            {'name': 'laugh'},
            {'name': 'look'},
            {'name': 'music'},
            {'name': 'rofl'},
            {'name': 'sad'},
            {'name': 'scratch'},
            {'name': 'shock'},
            {'name': 'sick'},
            {'name': 'smile'},
            {'name': 'smoke'},
            {'name': 'stuk'},
            {'name': 'v'},
            {'name': 'zlo'},
            {'name': 'zst'}
        ];

        return smilies;

    },

    dataURItoBlob: function (dataURI, mime) {


        var byteString = window.atob(dataURI);


        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }


        var blob = new Blob([ia], {type: mime});

        return blob;
    },

    makeid: function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    SendData: function (data, success, fail) {

        WinJS.xhr({
            url: MainApp.HostUrl + '/mobile.php',
            type: 'POST',
            data: data,
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        }).done(
            function completed(request) {
                if (request.status === 200) {

                    success(request.responseText);
                }
                else {


                    setTimeout(function () {

                        WinJS.xhr({
                            url: MainApp.HostUrl + '/mobile.php',
                            type: 'POST',
                            data: data,
                            headers: {
                                "content-type": "application/x-www-form-urlencoded"
                            }
                        }).done(
                            function completed(request_repeat) {
                                if (request_repeat.status === 200) {

                                    success(request_repeat.responseText);
                                }
                                else {
                                    fail();
                                }
                            },
                            function error(request) {
                                fail();
                            }
                        );


                    }, 7000);


                }
            },
            function error(request) {
                setTimeout(function () {

                    WinJS.xhr({
                        url: MainApp.HostUrl + '/mobile.php',
                        type: 'POST',
                        data: data,
                        headers: {
                            "content-type": "application/x-www-form-urlencoded"
                        }
                    }).done(
                        function completed(request_repeat) {
                            if (request_repeat.status === 200) {

                                success(request_repeat.responseText);
                            }
                            else {
                                fail();
                            }
                        },
                        function error(request) {
                            fail();
                        }
                    );


                }, 15000);
            }
        );


    },


    UpdateNotifications:function(newNotifications)
    {
        var saveNotifications = newNotifications;
        newNotifications = JSON.parse(newNotifications);
        var notifications = JSON.parse(Storage.get('notifications'));
        var oldMessages = 0;
        var newMessages = 0;
        for (o in notifications) {
            if (notifications[o].is_new == '1') {
                oldMessages++;
            }
        }

        for (a in newNotifications) {
            if (newNotifications[a].is_new == '1') {
                newMessages++;
            }
        }

        Storage.add('notifications', saveNotifications);
      
        if (newMessages > oldMessages)
        {
            if (oldMessages == 0) {
                MainApp.ShowNotifications();
                navigator.notification.beep(function () { });
            }
            else {

                $('.user_notifications').html(newMessages);
                navigator.notification.beep(function () { });
            }
        }
        else if (newMessages < oldMessages) {
            if (newMessages != 0)
            {
                $('.user_notifications').html(newMessages);
            }
            else {

                $('.user_notifications').remove();
            }


           
        }
        


      
     
        

    },


    ClearNotifications: function () {


        $('.app_container .user_notifications').remove();
        
        MainApp.SendData('do=readnotices', function (response) {

            Storage.add('notifications',response);

        }, function () { });



    },

    ShowNotifications: function () {


       
        var notifications = JSON.parse(Storage.get('notifications'));

        
       

        var count = Object.keys(notifications).length;

        var newNotifications = 0;


      

        for (o in notifications)
        {
            notifications[o].message = MainApp.parseNotifications(notifications[o].message, notifications[o].id);
            if (notifications[o].is_new == '1')
            {
                newNotifications++;
            }
        }

       
        if (newNotifications > 0)
        {
          
            MainApp.loadTemplate('notifications.html', {}, {count:newNotifications}, '.app_container', 'append', function () {

                $('.user_notifications').scaleAble();
                setTimeout(function () {


                   
                        $('.user_notifications').addClass('hvr-push');


                        setTimeout(function () {
                            $('.user_notifications').removeClass('hvr-push');
                        }, 500);
                   

                }, 500);


                $('.user_notifications').kt(function (a, b) {


                    WinJS.UI.SettingsFlyout.showSettings("options", "settings.html");
                   
                    setTimeout(function () {

                            MainApp.loadWidget('notifications', '.pagecontrol .win-settingsflyout', {}, { notifications: notifications}, function () {

                                MainApp.ClearNotifications();


                                document.getElementById("options").addEventListener("afterhide", function () {


                                    $('.pagecontrol .win-settingsflyout').html('<progress style="width:100%;"></progress>');


                                }, false);

                            }, function () { });







                    }, 1500);


                });


            
            });
        }

    },

    ShowModuleLoader: function () {

        $('.module_loader').remove();
        $('.app_container').prepend('<progress class="module_loader"></progress>');
        $('.main').css('visibility', 'hidden');


    },
    HideModuleLoader: function () {

        $('.module_loader').remove();
        $('.main').css('visibility', 'visible');


    },

    ShowAppLoader: function () {

        $('.module_loader').remove();
        $('.app_container').prepend('<progress class="module_loader"></progress>');
       


    },
    HideAppLoader: function () {

        $('.module_loader').remove();
      

    },



    ParseImages:function(content)
    {
        var elem = document.createElement("div");
        elem.innerHTML = content;

        $('img', elem).each(function () {


            if ($(this).attr('data-smile') == "true") {

                var src = $(this).attr('src');
                var sl = src.lastIndexOf('/') + 1;

                src = src.substring(sl);

                el = src.length - 4;


                src = src.substr(0, el);

                $(this).replaceWith(' :' + src + ': ');
            }
            else {

                var src = $(this).attr('src');


                $(this).replaceWith(' [img]' + src + '[/img] ');


            }

        });


  




        return elem.innerHTML;
    },


    ParseHtmlTags:function(content)
    {
        var elem = document.createElement("div");
        elem.innerHTML = content;
    

        $('div', elem).each(function () {


            var text = $(this).html();

            $(this).replaceWith(text + '\n');



        });




        return elem.innerHTML;


    },

    ParseWallContent: function (content) {

         var message = MainApp.ParseImages(content);
     
         message = MainApp.ParseHtmlTags(message);

         message = message.replace(/\&nbsp\;/gi, ' ');
       

         

        return message;

    },

    loadComponent: function (component, Tpldata, TplOtherData, success) {


        var componentname = component,
            componet_var = componentname.charAt(0).toUpperCase() + componentname.slice(1) + '_component',
            position = '.app_container',
            style = document.createElement("link");


        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", "css/components/" + componentname + "/index.css");
        style.setAttribute("data-added", "1");
        document.head.appendChild(style);


        MainApp.loadTemplate("components/" + componentname + "/index.html", Tpldata, TplOtherData, position, 'html', function () {



            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('src', "components/" + componentname + "/component.js");
            scriptElem.setAttribute('data-component', componentname);
            scriptElem.onload = function () {
                
                var Component_obj = eval(componet_var);


                $('body').css('background-color', Component_obj.background_color);

                if (Component_obj.showLoader == true) {
                    MainApp.showMainLoader(component, Component_obj.LoaderColor);
                }


                if (Object.keys(Component_obj.all_modules).length != 0) {


                    MainApp.foreachForModules(0, Component_obj.all_modules, function () {

                        setTimeout(function () {



                            $('[data-animation]').each(function () {



                                $(this).addClass($(this).attr('data-animation'));

                            });


                            $('[data-scale = true]').scaleAble();

                            setTimeout(function () {
                                $('.show_screen').hide();
                                success();

                                Component_obj.Init();

                            }, 1);

                        }, 10);

                    });

                }
                else {
                    setTimeout(function () {

                        $('[data-animation]').each(function () {


                            $(this).addClass($(this).attr('data-animation'));

                        });


                        $('[data-scale = true]').scaleAble();

                        setTimeout(function () {
                            $('.show_screen').hide();
                            success();
                            Component_obj.Init();

                        }, 1);

                    }, 10);


                }


            };
            if (document.body) {
                document.body.appendChild(scriptElem);
            } else {
                document.head.appendChild(scriptElem);
            }


            $('script[data-component=' + componentname + ']').remove();






        });

    },

    loadTemplate: function (url, data, other_data, position, type, success) {
        $.get('templates/' + url, function (Htmldata) {


            template_id = 'new_' + MainApp.makeid();


            var script = document.createElement("script");
            script.innerHTML = Htmldata;
            script.setAttribute("id", template_id);
            script.setAttribute("type", "text/x-jsrender");
            document.head.appendChild(script);


            if (type == 'html') {
                $(position).html(
                    $("#" + template_id).render(data, other_data)
                );
            }
            else if (type == 'append') {
                $(position).append(
                    $("#" + template_id).render(data, other_data)
                );
            }
            else if (type == 'prepend') {

                $(position).prepend(
                    $("#" + template_id).render(data, other_data)
                );
            }


            $('#' + template_id).remove();

            success();

        });


    },

    loadModule: function (modulename, position, Tpldata, TplOtherData, success, onend) {

        style = document.createElement("link");

        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", "css/modules/" + modulename + "/index.css");
        style.setAttribute("data-added", "1");
        document.head.appendChild(style);


        MainApp.loadTemplate("modules/" + modulename + "/index.html", Tpldata, TplOtherData, position, 'html', function () {


            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('src', "modules/" + modulename + "/module.js");
            scriptElem.setAttribute('data-module', modulename);
            scriptElem.onload = function () {
                success();
                onend();

            };
            if (document.body) {
                document.body.appendChild(scriptElem);
            } else {
                document.head.appendChild(scriptElem);
            }


            $('script[data-module='+modulename+']').remove();

           
                

          

        });
    },

    loadWidget: function (widgetname, position, Tpldata, TplOtherData, success, onend) {

        style = document.createElement("link");

        style.setAttribute("rel", "stylesheet");
        style.setAttribute("href", "css/widgets/" + widgetname + "/index.css");
        style.setAttribute("data-added", "1");
        document.head.appendChild(style);


        MainApp.loadTemplate("widgets/" + widgetname + "/index.html", Tpldata, TplOtherData, position, 'html', function () {





            var scriptElem = document.createElement('script');
            scriptElem.setAttribute('src', "widgets/" + widgetname + "/widget.js");
            scriptElem.setAttribute('data-widget', widgetname);
            scriptElem.onload = function () {
                success();
                onend();

            };
            if (document.body) {
                document.body.appendChild(scriptElem);
            } else {
                document.head.appendChild(scriptElem);
            }


            $('script[data-widget=' + widgetname + ']').remove();



           

        });
    },

    foreachForModules: function (i, modules, success) {
        var count = Object.keys(modules).length - 1;


        var module = modules[i];
        if (i < count) {

            MainApp.loadModule(module.module_name, module.position, module.TplData, module.OtherData, module.success, function () {

                if (Object.keys(module.widgets).length == 0) {
                    MainApp.foreachForModules(i + 1, modules, success);
                }
                else {
                    MainApp.foreachForWidgets(0, module.widgets, function () {

                        MainApp.foreachForModules(i + 1, modules, success);

                    });
                }


            });
        }
        else {

            MainApp.loadModule(module.module_name, module.position, module.TplData, module.OtherData, module.success, function () {

                if (Object.keys(module.widgets).length == 0) {


                    success();


                }
                else {
                    MainApp.foreachForWidgets(0, module.widgets, function () {


                        success();


                    });
                }

            });


        }

    },

    foreachForWidgets: function (i, widgets, success) {
        var count = Object.keys(widgets).length - 1;


        var widget = widgets[i];


        if (i < count) {

            MainApp.loadWidget(widget.widget_name, widget.position, widget.TplData, widget.OtherData, widget.success, function () {

                MainApp.foreachForWidgets(i + 1, widgets, success);

            });
        }
        else {
            MainApp.loadWidget(widget.widget_name, widget.position, widget.TplData, widget.OtherData, widget.success, function () {

                success();

            });

        }

    },

    uploadImage: function (fileUri, is_bloob, success) {

        if (!is_bloob) {


            var uri = new Windows.Foundation.Uri(fileUri);


            Windows.Storage.StorageFile.getFileFromApplicationUriAsync(uri).done(function (file) {


                file.openReadAsync().done(function (fileStream) {
                    var fileData = MSApp.createBlobFromRandomAccessStream(file.contentType, fileStream);

                    var formData = new FormData();
                    formData.append('picture', fileData, file.name);
                    formData.append('token', Storage.get('token'));
                    formData.append('do', 'uploadImage');

                    WinJS.xhr({
                        type: "post",
                        url: MainApp.HostUrl + "/mobile.php",
                        data: formData,
                        headers: {"Content-type": "multipart/form-data"}


                    }).done(function (result) {
                        success(result);
                    });

                });


            });
        }
        else {



            var formData = new FormData();

            formData.append('do', 'uploadImage'); // Append extra data before send.
            formData.append('token', Storage.get('token')); // Append extra data before send.
            formData.append('picture',fileUri); // Append extra data before send.

            var xhr = new XMLHttpRequest();
            xhr.open('POST', MainApp.HostUrl + "/mobile.php", true);
            xhr.onload = function(e) {

                success(e);

            };

            xhr.send(formData);


        }


    },

    uploadAvatar: function (fileUri, is_bloob, success) {

        if (!is_bloob) {


            var uri = new Windows.Foundation.Uri(fileUri);


            Windows.Storage.StorageFile.getFileFromApplicationUriAsync(uri).done(function (file) {


                file.openReadAsync().done(function (fileStream) {
                    var fileData = MSApp.createBlobFromRandomAccessStream(file.contentType, fileStream);

                    var formData = new FormData();
                    formData.append('picture', fileData, file.name);
                    formData.append('token', Storage.get('token'));
                    formData.append('do', 'avatar');

                    WinJS.xhr({
                        type: "post",
                        url: MainApp.HostUrl + "/mobile.php",
                        data: formData,
                        headers: {"Content-type": "multipart/form-data"}


                    }).done(function (result) {
                        success(result);
                    });

                });


            });
        }
        else {


            var fileData = this.response;
            var formData = new FormData();
            formData.append('picture', fileUri, 'khfsjdfgjsdf.jpg');
            formData.append('token', Storage.get('token'));
            formData.append('do', 'avatar');

            WinJS.xhr({
                type: "post",
                url: MainApp.HostUrl + "/mobile.php",
                data: formData,
                headers: {"Content-type": "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2)}


            }).done(function (result) {
                success(result);
            });


        }
    }


};

var User = {

    LoginUser: function (success, fail) {

        if (Storage.get('user')) {


            User.UpdateUserGlobalInfo(success, fail, function () {
                if (Storage.get('user')) {
                    success();
                }
                else {
                    fail();
                }
            });

        }
        else {
            fail();
        }

    },

    GetNewMessages: function () {

        
        $('.dialogs .contact').removeClass('hvr-push');


        if (navigator.onLine) {


            var us_id = JSON.parse(Storage.get('user')).id;


            var messages = JSON.parse(Storage.get('contacts'));
            var newMessages = 0;
            for (key in messages) {
                var msg_count = messages[key].new_msg;
                newMessages += msg_count;
            }


            MainApp.SendData('do=getnewmessages&in_user=' + us_id + '&old_count=' + newMessages, function (data) {



                

                var is_new = data.split('^')[0];

                var dataChat = data.split('^')[1];

                var notifications = data.split('^')[3];

                
                MainApp.UpdateNotifications(notifications);
                
                var isNewContact = false;


               



                /*ADD   NEW CONTACTS*/
                if (Object.keys(JSON.parse(Storage.get("contacts"))).length < Object.keys(JSON.parse(dataChat)).length)
                {


                    var isNewContact = true;


                    var olddChat = JSON.parse(Storage.get("contacts"));
                    var newChat = JSON.parse(dataChat);

                    var newAddedContacts = newChat;


                    for (key_of_new in newChat) {
                        for (key_of_old in olddChat) {
                            if (newChat[key_of_new].id == olddChat[key_of_old].id) {
                                newAddedContacts[key_of_new] = {};

                            }
                        }

                    }


                    var ChangedContacts = [];
                    var ip = 0;
                    for (i in newAddedContacts) {
                        if (newAddedContacts[i].id != undefined) {
                            ChangedContacts[ip] = newAddedContacts[i];
                            ip++;
                        }
                    }




                   MainApp.loadTemplate("modules/dialogs/index.html", ChangedContacts, {}, '.leftpanel .dialogs', 'append', function () {


                       var hasnew = false;
                       
                        for (i in ChangedContacts) {
                            if (ChangedContacts[i].new_msg > 0) {
                                $('.dialogs .contact[data-id = ' + ChangedContacts[i].id + ']').scaleAble();
                                $('.dialogs .contact[data-id = ' + ChangedContacts[i].id + ']').addClass('hvr-push');
                                
                                hasnew = true;

                            }
                            else {
                             
                                var newfriend = {
                                    id: ChangedContacts[i].id,
                                    nickname: ChangedContacts[i].nickname,
                                    login: "",
                                    avatar: ChangedContacts[i].avatar,
                                    flogdate: ""
                                };
                                
                                var friends = JSON.parse(Storage.get('friends'));
                                var new_i = Object.keys(friends).length;
                                friends[new_i] = newfriend;

                                Storage.add('friends', friends);
                                MainApp.loadWidget('user_friends', '.main .blocks .second_block .friends', {}, { friends: JSON.parse(Storage.get('friends')) }, function () {

                                    $('[data-scale = true]').scaleAble();

                                }, function () { });


                                }
                        }


                        if (hasnew) {
                            navigator.notification.beep(function () { });
                        }


                       $.get("modules/dialogs/module.js", function (data) {

                           eval(data);




                        });


                    });

               

                }
                /* /ADD   NEW CONTACTS*/


                /*SHOW NEW MESSAGES*/

                if (is_new != 'false') {
                   
                    var olddChat = JSON.parse(Storage.get("contacts"));
                    var newChat = JSON.parse(dataChat);

                    var activePage = MainApp.ActivePage.substr(0, 7);

                    if (activePage == 'Message') {
                        var activechat = MainApp.ActivePage.substr(8, MainApp.ActivePage.length);
                        
                    }
                    else {

                        var activechat = 0;
                        

                    }

                   

             

                    



                    if (Object.keys(olddChat).length < Object.keys(newChat).length) {
                        for (key_of_new in newChat) {
                            for (key_of_old in olddChat) {
                                if (olddChat[key_of_old].id == newChat[key_of_new].id && olddChat[key_of_old].id != activechat) {
                                    if (olddChat[key_of_old].new_msg != 0 && olddChat[key_of_old].new_msg != newChat[key_of_new].new_msg) {
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + '] .newmsg > div').html(newChat[key_of_new].new_msg);
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + ']').addClass('hvr-push');
                                        navigator.notification.beep(function () { });
                                        Storage.removeItem('messages' + olddChat[key_of_old].id);
                                    }
                                    else if (olddChat[key_of_old].new_msg == 0 && olddChat[key_of_old].new_msg != newChat[key_of_new].new_msg) {
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + '] .newmsg').html('<div>' + newChat[key_of_new].new_msg + '</div>');
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + ']').addClass('hvr-push');
                                        navigator.notification.beep(function () { });
                                        Storage.removeItem('messages' + olddChat[key_of_old].id);


                                    }

                                }
                                else if (olddChat[key_of_old].id == newChat[key_of_new].id && olddChat[key_of_old].id == activechat) {

                                    /*Show New Message*/

                                    var newMessagesCount = newChat[key_of_new].new_msg - olddChat[key_of_old].new_msg;
                                    if (newMessagesCount > 0) {
                                        MainApp.SendData('do=getnewmessagescontent&in_user=' + us_id + '&limit=' + newMessagesCount + '&with_user' + activechat, function (newMessagesContent) {

                                            var message_data = JSON.parse(newMessagesContent);
                                            $.get("templates/modules/messages/messages.html", function (Tpldata) {


                                                var script = document.createElement("script");
                                                script.innerHTML = Tpldata;
                                                script.setAttribute("id", "messages");
                                                script.setAttribute("type", "text/x-jsrender");
                                                document.head.appendChild(script);

                                                $(".main .messages_box .messages_list .show_messages").append(
                                                    $("#messages").render(message_data, {
                                                        in_us_id: us_id,
                                                        hvr: 'hvr-buzz-out'
                                                    })
                                                );

                                                $("#messages").remove();

                                                $('.main .messages_box .messages_list .show_messages').scrollTop($('.main .messages_box .messages_list .show_messages')[0].scrollHeight);

                                                $(".main .messages_box .messages_list .show_messages").css('background-image', 'none');

                                                $('.messages_box .messages_list .progress_bar').addClass('hiddenProgress');

                                                navigator.notification.beep(function () { });
                                                Storage.removeItem('messages' + activechat);

                                                var datanewChatedited = JSON.parse(dataChat);

                                                for (key_of_active in datanewChatedited) {
                                                    if (datanewChatedited[key_of_active].id == activechat) {
                                                        datanewChatedited[key_of_active].new_msg = 0;
                                                    }
                                                }

                                                dataChat = JSON.stringify(datanewChatedited);


                                            });

                                        }, function () {
                                        });
                                    }


                                    /*Show New Message*/

                                }
                            }
                        }
                    }
                    else {
                        for (key_of_old in olddChat) {
                            for (key_of_new in newChat) {
                                if (olddChat[key_of_old].id == newChat[key_of_new].id && olddChat[key_of_old].id != activechat) {
                                    if (olddChat[key_of_old].new_msg != 0 && olddChat[key_of_old].new_msg != newChat[key_of_new].new_msg) {
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + '] .newmsg > div').html(newChat[key_of_new].new_msg);
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + ']').addClass('hvr-push');
                                        navigator.notification.beep(function () { });
                                        Storage.removeItem('messages' + olddChat[key_of_old].id);

                                    }
                                    else if (olddChat[key_of_old].new_msg == 0 && olddChat[key_of_old].new_msg != newChat[key_of_new].new_msg) {
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + '] .newmsg').html('<div>' + newChat[key_of_new].new_msg + '</div>');
                                        $('.dialogs .contact[data-id = ' + olddChat[key_of_old].id + ']').addClass('hvr-push');
                                        navigator.notification.beep(function () { });
                                        Storage.removeItem('messages' + olddChat[key_of_old].id);


                                    }

                                }
                                else if (olddChat[key_of_old].id == newChat[key_of_new].id && olddChat[key_of_old].id == activechat) {
                                    /*Show New Message*/

                                    var newMessagesCount = newChat[key_of_new].new_msg - olddChat[key_of_old].new_msg;
                                    if (newMessagesCount > 0) {
                                        MainApp.SendData('do=getnewmessagescontent&in_user=' + us_id + '&limit=' + newMessagesCount + '&with_user=' + activechat, function (newMessagesContent) {

                                            var message_data = JSON.parse(newMessagesContent);
                                            $.get("templates/modules/messages/messages.html", function (Tpldata) {


                                                var script = document.createElement("script");
                                                script.innerHTML = Tpldata;
                                                script.setAttribute("id", "messages");
                                                script.setAttribute("type", "text/x-jsrender");
                                                document.head.appendChild(script);

                                                $(".main .messages_box .messages_list .show_messages").append(
                                                    $("#messages").render(message_data, {
                                                        in_us_id: us_id,
                                                        hvr: 'hvr-buzz-out'
                                                    })
                                                );

                                                $("#messages").remove();

                                                $('.main .messages_box .messages_list .show_messages').scrollTop($('.main .messages_box .messages_list .show_messages')[0].scrollHeight);

                                                $(".main .messages_box .messages_list .show_messages").css('background-image', 'none');

                                                $('.messages_box .messages_list .progress_bar').addClass('hiddenProgress');

                                                navigator.notification.beep(function () { });
                                                Storage.removeItem('messages' + activechat);

                                                var datanewChatedited = JSON.parse(dataChat);

                                                for (key_of_active in datanewChatedited) {
                                                    if (datanewChatedited[key_of_active].id == activechat) {
                                                        datanewChatedited[key_of_active].new_msg = 0;
                                                    }
                                                }

                                                dataChat = JSON.stringify(datanewChatedited);

                                            });

                                        }, function () {
                                        });
                                    }

                                    /*Show New Message*/
                                }
                            }
                        }

                    }


                }


                /* / SHOW NEW MESSAGES*/


                Storage.add('contacts', dataChat);

               


             


                setTimeout(function () {

                    User.GetNewMessages();


                }, 8000);


            }, function () {

            });


        }

    },

    UpdateUserGlobalInfo: function (success, fail, no_connection) {


        if (navigator.onLine) {
            var us_id = JSON.parse(Storage.get('user')).id;


            MainApp.SendData('do=autologin&id=' + us_id, function (data) {

             
                var VRegExp = new RegExp(/^(\s|\u00A0)+/g);


                if (data.replace(VRegExp, '') == "false") {

                    Storage.clear();
                    fail();


                }
                else {
                    Storage.clear();

                    Storage.add("user", data);




                    var data = JSON.parse(Storage.get("user"));


                    MainApp.SendData('do=getcontacts&us_id=' + data['id'], function (dataAll) {


                     

                        dataAll = JSON.parse(dataAll);
                        dataChat = dataAll[0];
                        dataFriends = dataAll[1];
                        dataWall = dataAll[2];
                        notifications = dataAll[3];
                        

                        Storage.add("contacts", dataChat);
                        Storage.add("friends", dataFriends);
                        Storage.add("wall", dataWall);
                        Storage.add("notifications", notifications);
                        for (var key in localStorage) {
                            if (key.substr(0, 8) == 'messages') {
                                Storage.removeItem(key);
                            }
                        }
                        success();

                    }, function () {


                        fail();

                    });
                    


                }


            }, function () {

                fail();

            });

        }
        else {
            no_connection();
        }


    }


};


var Messages = {

    ShowMessages: function (Contact_id) {


       

      if (MainApp.ActivePage != 'Message_' + Contact_id) {

           MainApp.ShowModuleLoader();
          


        

           $('.app_container .active').removeClass('active');
            MainApp.ActivePage = 'Message_' + Contact_id;
            $('.leftpanel .dialogs .contact[data-id = ' + Contact_id + ']').addClass('active');


            var with_user = Messages.GetWithUserInfo(Contact_id);
            var in_user = Messages.GetInUserInfo();
            var smilies = MainApp.getSmilies();
          

            MainApp.loadModule('messages', '.main', {}, {
                with_user: with_user,
                in_user: in_user,
                smilies: smilies,
                messages: {},
                in_us_id: JSON.parse(Storage.get('user')).id
            }, function () {


                Messages_module.Init(Contact_id);
                MainApp.HideModuleLoader();


            }, function () {
            });
            



       }

    },

    GetInUserInfo: function () {


        var chat_data = JSON.parse(Storage.get('user'));
        var TplData = {};


        var avatar = chat_data.avatar;
        avatar = avatar.replace('/small', '');
        TplData.in_user_avatar = avatar;
        TplData.in_user_nickname = chat_data.nickname;
        TplData.in_user_status = " ";

        return TplData;


    },

    GetWithUserInfo:function(Contact_id){

        var with_us_id = Contact_id;
        var chat_data = JSON.parse(Storage.get('contacts'));
        var TplData = {};
        for (var key in chat_data) {

            if (chat_data[key].id == with_us_id) {

                var avatar = chat_data[key].avatar;
                avatar = avatar.replace('/small', '');
                TplData.with_user_avatar = avatar;
                TplData.with_user_nickname = chat_data[key].nickname;
                TplData.with_user_status = " ";

            }

        }

        return TplData;



    },

    SendMessage: function (message) {





        if (MainApp.realLength(message).length > 0) {
            message = MainApp.StripHtml(message);
            var date = new Date();
            var mon = ('0' + (1 + date.getMonth())).replace(/.?(\d{2})/, '$1');
            var senddate = date.toString().replace(/^[^\s]+\s([^\s]+)\s([^\s]+)\s([^\s]+)\s([^\s]+)\s.*$/ig, '$3-' + mon + '-$2 $4');
            var activePage = MainApp.ActivePage.substr(0, 7);

            if (activePage == 'Message') {
                var to_id = MainApp.ActivePage.substr(8, MainApp.ActivePage.length);

            }
            else {

                var to_id = 0;


            }
            
            var from_id = JSON.parse(Storage.get('user')).id;
            var message_data = { from_id: from_id, message: message, senddate: senddate };




            $.get("templates/modules/messages/messages.html", function (data) {

                var script = document.createElement("script");
                script.innerHTML = data;
                script.setAttribute("id", "messages");
                script.setAttribute("type", "text/x-jsrender");
                document.head.appendChild(script);

                $(".main .messages_box .messages_list .show_messages").append(
                    $("#messages").render(message_data, {
                        in_us_id: from_id,
                        hvr: 'hvr-wobble-horizontal dont_sended'
                    })
                );

                $("#messages").remove();

                $('.main .messages_box .messages_list .show_messages').scrollTop($('.main .messages_box .messages_list .show_messages')[0].scrollHeight);


                $(".main .messages_box .messages_list .show_messages").css('background-image', 'url("none")')

                $('.main .input_box .text_message').html('');

                

                MainApp.SendData('do=sendmessage&from_id=' + from_id + '&to_id=' + to_id + '&message=' + message, function (response) {



                    $('.main .messages_box .messages_list .show_messages .dont_sended').removeClass('dont_sended');

                    Storage.removeItem('messages' + to_id);

                }, function () { });



            });

        }


    },



    SendImageMessage: function (image,isbloob) {

        var message = '<img  width="150" src="' + image.path + '" />';

        var date = new Date();
        var mon = ('0' + (1 + date.getMonth())).replace(/.?(\d{2})/, '$1');
        var senddate = date.toString().replace(/^[^\s]+\s([^\s]+)\s([^\s]+)\s([^\s]+)\s([^\s]+)\s.*$/ig, '$3-' + mon + '-$2 $4');
        var activePage = MainApp.ActivePage.substr(0, 7);

        if (activePage == 'Message') {
            var to_id = MainApp.ActivePage.substr(8, MainApp.ActivePage.length);

        }
        else {

            var to_id = 0;


        }

        var from_id = JSON.parse(Storage.get('user')).id;
        var message_data = { from_id: from_id, message: message, senddate: senddate };




        $.get("templates/modules/messages/messages.html", function (data) {

            var script = document.createElement("script");
            script.innerHTML = data;
            script.setAttribute("id", "messages");
            script.setAttribute("type", "text/x-jsrender");
            document.head.appendChild(script);

            $(".main .messages_box .messages_list .show_messages").append(
                $("#messages").render(message_data, {
                    in_us_id: from_id,
                    hvr: 'hvr-wobble-horizontal dont_sended'
                })
            );

            $("#messages").remove();

            $('.main .messages_box .messages_list .show_messages').scrollTop($('.main .messages_box .messages_list .show_messages')[0].scrollHeight);


            $(".main .messages_box .messages_list .show_messages").css('background-image', 'url("none")')


            MainApp.uploadImage(image,isbloob,function (response) {


                var newimage = '/upload/users/' + response.target.responseText;

               

                var newmessage = '<img width="150" src="' + newimage + '" />';


                MainApp.SendData('do=sendmessage&from_id=' + from_id + '&to_id=' + to_id + '&message=' + newmessage, function (response) {



                    $('.main .messages_box .messages_list .show_messages .dont_sended').removeClass('dont_sended');

                    Storage.removeItem('messages' + to_id);

                }, function () { });
                

            });


        });






    },


    GetMessages:function(Contact_id,success){

        var in_us_id = JSON.parse(Storage.get('user')).id;

        var formData = 'do=getmessages&in_user=' + in_us_id + '&with_user=' + Contact_id + '&page=' + 0 + '&perpage=' + 0;

        if (!Storage.get('messages' + Contact_id)) {
            MainApp.SendData(formData, function (message_data) {

                success(JSON.parse(message_data));
                $('.dialogs .active .newmsg > div').remove();
                var activechat = Contact_id;

                if (MainApp.ActivePage == 'Messages_' + Contact_id) {
                    var contacts_for_edit = JSON.parse(Storage.get('contacts'));

                    for (key_of_contact in contacts_for_edit) {
                        if (contacts_for_edit[key_of_contact].id == activechat) {
                            contacts_for_edit[key_of_contact].new_msg = 0;
                        }
                    }


                    Storage.add('contacts', JSON.stringify(contacts_for_edit));
                   

                }
                else {
                    message_data = JSON.parse(message_data);
                    if (message_data.length != 0) {
                        Storage.add('messages' + Contact_id, message_data);
                    }
                    else {
                        Storage.add('messages' + Contact_id, { data: 'empty' });

                    }

                }






            }, function () {
                navigator.notification.alert('Ошибка соединения', function () {
                }, " ");
            });
        }
        else {

            var message_data = JSON.parse(Storage.get('messages' + Contact_id));

            success(message_data);
           

        }




    }


};




var Storage = {



    clear: function () {
        for (var key in localStorage) {
            if (key != 'token' || key != 'app_activity' || key != 'activePage') {
                localStorage.removeItem(key);
            }
        }

        
    },

    add: function (title, object) {

        if (object != "") {
            if (typeof object == "string") {
                var data = object;

            }
            else {
                var data = JSON.stringify(object);

            }


        }
        else {
            var data = "{}";


        }

        try
        {
            var o = JSON.parse(data);
            o = JSON.stringify(o);
            localStorage.setItem(title, o);

        }
        catch(e)
        {
        }

        

       


    },


    removeItem: function (key) {

        localStorage.removeItem(key);



    },

    get: function (title) {

        

     

        if (localStorage.getItem(title) != null) {
            return localStorage.getItem(title);
        }
        else {
            return false;
        }

    }


};

$.fn.extend({
    scaleAble: function () {


        $(this).on('mousedown', function () {
            if (!$(this).hasClass('active')) {

                $(this).addClass('active_scale');

            }
        }).on('mouseup', function () {
            if (!$(this).hasClass('active')) {
                $(this).removeClass('active_scale');
            }
        }).hover(function () {
        }, function () {
            if (!$(this).hasClass('active')) {
                $(this).removeClass('active_scale');
            }
        });


    },
    kt: function (success) {

        $(this).on('click', function (a) {
            success(a, $(this));
        });

    }

});


navigator.popup = {


    showPopUp:function(obj,data,position){


        $('#menu1').remove();

        MainApp.loadTemplate('popups.html',{},{'context':data},'body','append',function(){

          console.log(obj);
            WinJS.UI.processAll().done(function () {

                    var menu = document.getElementById("menu1").winControl;

                    menu.show(obj,position);



            });



        });


       // navigator.notification.alert(JSON.stringify(data),function(){},'');






    }




};


navigator.camera = {




    takepicture: function (success, error) {

        var captureUI = new Windows.Media.Capture.CameraCaptureUI();
        captureUI.photoSettings.format = Windows.Media.Capture.CameraCaptureUIPhotoFormat.jpeg;


        WinJS.UI.processAll().then(function () {
   

        captureUI.captureFileAsync(Windows.Media.Capture.CameraCaptureUIMode.photo).then(function (picture) {
            if (picture) {

                var storageFolder = Windows.Storage.ApplicationData.current.localFolder;
                picture.copyAsync(storageFolder, picture.name, Windows.Storage.NameCollisionOption.replaceExisting).then(function (storageFile) {

                    success("ms-appdata:///local/" + storageFile.name);


                }, function () {
                    error();
                });



            }
            else {
                error();
            }
        });

        });

    },

    getPicture: function (success,error) {


        var fileOpenPicker = new Windows.Storage.Pickers.FileOpenPicker();
        fileOpenPicker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.picturesLibrary;
        fileOpenPicker.fileTypeFilter.replaceAll([".png", ".jpg", ".jpeg"]);


        WinJS.UI.processAll().then(function () {
            fileOpenPicker.pickSingleFileAsync().done(function (file) {

                if (file) {
                    var storageFolder = Windows.Storage.ApplicationData.current.localFolder;
                    file.copyAsync(storageFolder, file.name, Windows.Storage.NameCollisionOption.replaceExisting).then(function (storageFile) {
                        success(URL.createObjectURL(storageFile));
                    }, function () {
                        error();
                    });
                }
                else {

                    error();

                }


            });

        });








    },

    takefile: function (success, error) {


        var fileOpenPicker = new Windows.Storage.Pickers.FileOpenPicker();
        fileOpenPicker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.picturesLibrary;
        fileOpenPicker.fileTypeFilter.replaceAll([".zip", ".rar", ".pdf"]);


        WinJS.UI.processAll().then(function () {
            fileOpenPicker.pickSingleFileAsync().done(function (file) {

                if (file) {
                    var storageFolder = Windows.Storage.ApplicationData.current.localFolder;
                    file.copyAsync(storageFolder, file.name, Windows.Storage.NameCollisionOption.replaceExisting).then(function (storageFile) {
                        success(URL.createObjectURL(storageFile));
                    }, function () {
                        error();
                    });
                }
                else {

                    error();

                }


            });

        });








    }

};



navigator.notification = {

     isAlertShowing : false,
     alertStack : [],


    alert: function (message, success, title) {

        if (navigator.notification.isAlertShowing) {
            var later = function () {
                navigator.notification.alert(message, success, title);
            };
            navigator.notification.alertStack.push(later);
            return;
        }
        navigator.notification.isAlertShowing = true;

        
     // alert(message);

        MainApp.loadTemplate('alert.html',{},{'message':message},'.app_container','append',function(){


                    $('body > .app_container .main_alert_box .alert_box .alert_content .alert_close_button').kt(function(){


                        $('body > .app_container .main_alert_box').remove();
                        success();
                        navigator.notification.isAlertShowing = false;



                        if (navigator.notification.alertStack.length) {
                            setTimeout(navigator.notification.alertStack.shift(), 0);
                        }




                    });




        });



    },
    

    beep: function (onEvent) {


        var audio = document.getElementsByTagName('audio')[0];



        audio.play();
            
        onEvent();


    }



};

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
}

var is_loaded = getURLParameter('loadscript');
if (is_loaded == '1') {
    MainApp.InitPage();
}
else
{
    MainApp.InitPage();
}

