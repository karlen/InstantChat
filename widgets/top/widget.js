﻿var Top_widget = {




    Logout: function () {
        MainApp.ShowAppLoader();

        for (var i = 1; i < 10; i++) {
            window.clearInterval(i);
            window.clearTimeout(i);

        }

        Storage.clear();



        MainApp.SendData('do=logout', function (response) {

            window.location.href = 'index.html?loadscript=1';


        }, function () { });
    },


    ShowMyNotifications: function () {
        WinJS.UI.SettingsFlyout.showSettings("options", "settings.html");

        setTimeout(function () {
            var notifications = JSON.parse(Storage.get('notifications'));

            MainApp.loadWidget('notifications', '.pagecontrol .win-settingsflyout', {}, {notifications: notifications}, function () {

                MainApp.ClearNotifications();


                document.getElementById("options").addEventListener("afterhide", function () {


                    $('.pagecontrol .win-settingsflyout').html('<progress style="width:100%;"></progress>');


                }, false);


            }, function () {
            });


        }, 1500);

    },


    Init: function () {


        $('.main .top .top_wrap .settings_button').click(function (clickArgs, a) {


            var firstContext = {'title': 'Уведомления', 'function': 'Top_widget.ShowMyNotifications();'};
            var secondContext = {'title': "Выход", 'function': 'Top_widget.Logout();'};

            var Context = [firstContext, secondContext];


            navigator.popup.showPopUp(this, Context, 'bottom');



        });




    }


}

Top_widget.Init();