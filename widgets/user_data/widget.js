﻿var User_data_widget = {

    Init: function () {


        $('.main .blocks .first_block .user_data_wrap .change_avatar table .camera').kt(function (e, obj) {



            navigator.camera.takepicture(function (picture) {




                if(picture)
                {



               $('span', obj).css('visibility', 'hidden');
                obj.css('background-image', ' url("images/loader1.GIF")');




                    $('span', obj).css('visibility', 'hidden');
                    obj.css('background-image', ' url("images/loader1.GIF")');






                    var formData = new FormData();

                    formData.append('do', 'avatar'); // Append extra data before send.
                    formData.append('token', Storage.get('token')); // Append extra data before send.
                    formData.append('picture',picture,'blob.png'); // Append extra data before send.

                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', MainApp.HostUrl + "/mobile.php", true);
                    xhr.onload = function(e) {


                        User_data_widget.ChangeAvatar(e.target.responseText,obj);

                    };

                    xhr.send(formData);


                }






            }, function () { });



        });
       


        $('.main .blocks .first_block .user_data_wrap .change_avatar table .from_file').kt(function (e,obj) {


            $('#file_picker').remove();

            var input=document.createElement('input');
            input.type="file";
            input.id = "file_picker";
            input.hidden = 'hidden';
            input.accept= "image/x-png, image/gif, image/jpeg";
            document.getElementsByTagName('body')[0].appendChild(input);


            $(input).click();

            $(input).on('change',function(evt){
                $('span', obj).css('visibility', 'hidden');
                obj.css('background-image', ' url("images/loader1.GIF")');

                evt.stopPropagation();
                evt.preventDefault();


                var file = evt.target.files[0];
                var formData = new FormData();

                formData.append('do', 'avatar'); // Append extra data before send.
                formData.append('token', Storage.get('token')); // Append extra data before send.
                formData.append('picture',file); // Append extra data before send.

                var xhr = new XMLHttpRequest();
                xhr.open('POST', MainApp.HostUrl + "/mobile.php", true);
                xhr.onload = function(e) {

                           User_data_widget.ChangeAvatar(e.target.responseText,obj);

                };

                xhr.send(formData);





            });




         

        });


    },

    ChangeAvatar:function(url,obj){

        $.get(MainApp.HostUrl+'/images/users/avatars/'+url,function(){


            $('.leftpanel .in_user .user_wrap .avatar').css('background-image','url('+MainApp.HostUrl+'/images/users/avatars/'+url+')');

            var User_Data = JSON.parse(Storage.get('user'));
            var oldImage = User_Data.avatar;

            User_Data.avatar = "/images/users/avatars/" + url;
            User_Data = JSON.stringify(User_Data);
            Storage.add('user',User_Data);




            var wall = JSON.parse(Storage.get('wall'));


            for(var key in wall){

                var img = wall[key].avatar;

                img = img.replace('/images/users/avatars/small/','');

                var old_img = oldImage.replace('/images/users/avatars/','');




                if(old_img == img)
                {
                    wall[key].avatar = '/images/users/avatars/small/'+url;
                }

            }


            Storage.add('wall',wall);




            if(MainApp.ActivePage == 'profile')
            {
                $('span',obj).css('visibility','visible');
                obj.css('background-image','none');




                $('.main .blocks .first_block .user_data_wrap .avatar > img').attr('src',MainApp.HostUrl+'/images/users/avatars/'+url);

                MainApp.loadWidget('user_wall','.main .blocks .second_block .wall',{},{wall: JSON.parse(Storage.get('wall')), smilies: MainApp.getSmilies()},function(){

                },function(){});

            }






        });




    }




}

User_data_widget.Init();