﻿var User_friends_widget = {

    openProfileInBrowser:function(login){


        gui.Shell.openExternal(MainApp.HostUrl + '/users/' + login);
       // window.open(MainApp.HostUrl + '/users/' + login, '_system');


    },

    openProfileSettings:function(us_id)
    {

        WinJS.UI.SettingsFlyout.showSettings("options", "settings.html");
        setTimeout(function () {

            MainApp.SendData('do=getFriendInfo&friend_id=' + us_id, function (response) {


                var TplData = JSON.parse(response);

                MainApp.loadWidget('friend_info', '.pagecontrol .win-settingsflyout', {}, { friend_data: TplData[0], friendlist: TplData[1], walllist: TplData[2] }, function () {

                    var wdth = 90 * Object.keys(TplData[1]).length;

                    $(".friend_data_wrap .user_friends_wrap .friendslist .friends_wrap").css('width', wdth + 'px');

                    $(".friend_data_wrap .add_wall_post_friend").scaleAble();


                    $(".friend_data_wrap .user_friends_wrap .friendslist").mousewheel(function (event, delta) {

                        var sc = $('.friend_data_wrap .user_friends_wrap .friendslist').scrollLeft();
                        sc -= (delta * 15);

                        $('.friend_data_wrap .user_friends_wrap .friendslist').scrollLeft(sc);

                        event.preventDefault();

                    });



                    document.getElementById("options").addEventListener("afterhide", function(){


                        $('.pagecontrol .win-settingsflyout').html('<progress style="width:100%;"></progress>');


                    }, false);



                }, function () { });


            }, function () { });





        }, 1500);

    },












    Init: function () {


        var wdth = 180 * Object.keys(JSON.parse(Storage.get('friends'))).length;

        $(".main .blocks .second_block .friends .user_friends_wrap .friendslist .friends_wrap").css('width', wdth + 'px');

        $(".main .blocks .second_block .friends .user_friends_wrap .friendslist").mousewheel(function (event, delta) {

            var sc = $('.main .blocks .second_block .friends .user_friends_wrap .friendslist').scrollLeft();
            sc -= (delta * 15);

            $('.main .blocks .second_block .friends .user_friends_wrap .friendslist').scrollLeft(sc);

            event.preventDefault();

        });


        $('.main .blocks .second_block .friends .user_friends_wrap .friendslist .friends_wrap .friend_it').mouseup(function (clickArgs) {


            var obj = $(this);

            var nickname = obj.attr('data-title');
            var us_id = obj.attr('data-id');
            var login = obj.attr('data-login');

            var firstContext = {'title':nickname,'function':'User_friends_widget.openProfileSettings('+us_id+');'};
            var secondContext = {'title':"Открыть профиль",'function':'User_friends_widget.openProfileInBrowser("'+login+'");'};
            var thirdContext = {'title':"Открыть переписку",'function':'Messages.ShowMessages('+us_id+')'};

            var Context = [firstContext,secondContext,thirdContext];

            navigator.popup.showPopUp(this,Context,'right');

        });


    }

}

User_friends_widget.Init();