﻿var User_wall_widget = {


    bought: '',


    showAddWallContent: function () {
        User_wall_widget.bought = false;
        document.getElementById("AddWallPostContent").winControl.show(add_post);
       

    },

    deleteWallPost:function(id)
    {
        MainApp.ShowAppLoader();
       

        MainApp.SendData('do=delwall&id=' +id, function (response) {


            

            var wall = JSON.parse(response);

            Storage.add('wall', wall);



           


            $('.wall_it[data-id="'+id+'"]').hide(700);
            MainApp.HideAppLoader();
            setTimeout(function(){
                MainApp.loadWidget('user_wall', '.main .blocks .second_block .wall', {}, { wall: JSON.parse(Storage.get('wall')), smilies: MainApp.getSmilies() }, function () {
    
                    MainApp.HideAppLoader();
    
                    $('[data-scale="true"]').scaleAble();
    
    
                }, function () { });
            
            },1000);
          



        }, function () { });


    },

    addWallImage: function (image,isbloob,obj) {

        $('span', obj).css('visibility', 'hidden');
        obj.css('background-image', ' url("images/loader1.GIF")').css('background-position', 'center').css('background-size', '100%').css('background-repeat', 'no-repeat');
        var messgae = $('#AddWallPostContent .wall_add_content').html();
        MainApp.uploadImage(image, isbloob, function (response) {

            var smile = '<img unselectable="on" src="' + MainApp.HostUrl + '/upload/users/' + response.target.responseText + '" style="max-width:320px;box-sizing:border-box;clear:both"/>\n';
            messgae += smile;
           
            $('#AddWallPostContent .wall_add_content').html(messgae);

            $('span', obj).css('visibility', 'visible');
            obj.css('background-image', 'none');



        });


       






    },


    Init: function () {



        $(".main .blocks .second_block .wall .user_wall_wrap .walllist").mousewheel(function (event, delta) {

            var sc = $('.main .blocks .second_block .wall .user_wall_wrap .walllist').scrollLeft();
            sc -= (delta * 15);

            $('.main .blocks .second_block .wall .user_wall_wrap .walllist').scrollLeft(sc);

            event.preventDefault();

        });



        $(".main .blocks .second_block .wall .user_wall_wrap .walllist .wall_it .wall_content ").mousewheel(function (event, delta) {

            if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
            {
                event.preventDefault();
            }
            else {
                event.stopPropagation();
            }

        });



        /*Open Wall Post PopUp*/
        WinJS.UI.processAll().then(function () {

            document.getElementById("add_post").addEventListener("click", User_wall_widget.showAddWallContent, false);

        });
        /*Open Wall Post PopUp*/


        /*Smilies Container*/
        var smiles = MainApp.getSmilies();
        var count = Math.ceil(Object.keys(smiles).length / 2);
        var swdth = 50 * count;


        $('#AddWallPostContent .smilies .smilieslist').css('width', swdth + 'px');


        $("#AddWallPostContent .smilies").mousewheel(function (event, delta) {

            var sc = $('#AddWallPostContent .smilies').scrollLeft();
            sc -= (delta * 15);

            $('#AddWallPostContent .smilies').scrollLeft(sc);

            event.preventDefault();

        });


        $('#AddWallPostContent .smilies .smilieslist .smile_it').kt(function (a, b) {

            var messgae = $('#AddWallPostContent .wall_add_content').html();
            var smile = '<img unselectable="on"  data-smile = "true" src="' + b.attr('data-url') + '" />&nbsp;';
            messgae += smile;
            $('#AddWallPostContent .wall_add_content').html(messgae).focus();

        });

        /*Smilies Container*/


        /*WallList Container*/

        var wdth = (380 * Object.keys(JSON.parse(Storage.get('wall'))).length);

        $(".main .blocks .second_block .wall .user_wall_wrap .walllist .wall_wrap").css('width', wdth + 'px');

        $('#AddWallPostContent .wall_add_content').attr('contenteditable', 'true');

        $('#AddWallPostContent .wall_add_content').on('keydown keyup input  DOMNodeInserted DOMNodeRemoved', function () {

            if (!$(this).text()) {
                $(this).attr('data-placeholder', 'Введите текст...');
            }
            else {
                $(this).removeAttr('data-placeholder');
                
            }
        });


        $("#AddWallPostContent .attacha_from_file").kt(function (a, b) {



            $('#file_picker').remove();

            var input=document.createElement('input');
            input.type="file";
            input.id = "file_picker";
            input.hidden = 'hidden';
            input.accept= "image/x-png, image/gif, image/jpeg";
            document.getElementsByTagName('body')[0].appendChild(input);

            $(input).on('click',function(){
                setTimeout(function(){
                    User_wall_widget.showAddWallContent();
                },500);


            });

            $(input).click();




            $(input).on('change',function(evt) {

                var file = evt.target.files[0];
                User_wall_widget.addWallImage(file,1,b);


            });









        });


        $("#AddWallPostContent .attacha_from_camera").kt(function (a, b) {


            navigator.camera.takepicture(function (image) {



                if (image) {

                    User_wall_widget.addWallImage(image, 0,b);
                    setTimeout(function(){
                    User_wall_widget.showAddWallContent();
                    },1000);
                  
                }

            }, function () {
                User_wall_widget.showAddWallContent();
            });



        });



        $("#AddWallPostContent .attacha_file").kt(function (a, b) {


            navigator.camera.takefile(function (file) {

                User_wall_widget.showAddWallContent();

                if (file) {


                    // Messages.SendImageMessage(image, 1);
                }

            }, function () {
                User_wall_widget.showAddWallContent();
            });



        });



        $('#AddWallPostContent .set_wall_post').kt(function () {



            var content = $('#AddWallPostContent .wall_add_content').html();

            if (MainApp.realLength(content).length > 0) {

                MainApp.ShowAppLoader();


                content = MainApp.ParseWallContent(content);


               

                MainApp.SendData('do=addwall&content=' + content, function (response) {



                    var wall = JSON.parse(response);

                    Storage.add('wall', wall);

                  
                        MainApp.loadWidget('user_wall','.main .blocks .second_block .wall',{},{wall: JSON.parse(Storage.get('wall')), smilies: MainApp.getSmilies()},function(){
                        
                            MainApp.HideAppLoader();

                            $('[data-scale="true"]').scaleAble();

                        
                        }, function () { });



                }, function () { });


            
              
            }


        });


        $('.main .blocks .second_block .wall .user_wall_wrap .walllist .wall_wrap .wall_it .wall_settings').click(function () {



            var e = $(this);
            var id = e.attr('data-id');


            var firstContext = {'title': 'Удалить', 'function': ' User_wall_widget.deleteWallPost('+id+');'};
            var secondContext = {'title': 'Отметить', 'function': ''};


            var Context = [firstContext,secondContext];


            navigator.popup.showPopUp(this, Context, 'bottom');


           



        });





        /*WallList Container*/


    }


}


User_wall_widget.Init();





